<?php

namespace Drupal\simple_oauth_claims\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\simple_oauth_claims\ClaimInterface;

/**
 * Defines the claim entity type.
 *
 * @ConfigEntityType(
 *   id = "claim",
 *
 *   label = @Translation("Claim"),
 *   label_collection = @Translation("Claims"),
 *   label_singular = @Translation("claim"),
 *   label_plural = @Translation("claims"),
 *   label_count = @PluralTranslation(
 *     singular = "@count claim",
 *     plural = "@count claims",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\simple_oauth_claims\ClaimListBuilder",
 *     "form" = {
 *       "add" = "Drupal\simple_oauth_claims\Form\ClaimForm",
 *       "edit" = "Drupal\simple_oauth_claims\Form\ClaimForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "claim",
 *   admin_permission = "administer claim",
 *   links = {
 *     "collection" = "/admin/structure/claims",
 *     "add-form" = "/admin/structure/claims/add",
 *     "edit-form" = "/admin/structure/claims/{claim}",
 *     "delete-form" = "/admin/structure/claims/{claim}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "type",
 *     "field_name",
 *     "status"
 *   }
 * )
 */
class Claim extends ConfigEntityBase implements ClaimInterface {

  const TYPE_OIDC = 'oidc';

  const TYPE_PRIVATE = 'private';

  const TYPE_UND = 'undetermined';

  /**
   * The claim ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The claim label.
   *
   * @var string
   */
  protected $label;

  /**
   * The claim type.
   *
   * @var string
   */
  protected $type;

  /**
   * The name of the corresponding field.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Get type.
   *
   * @return string
   *   The claim type: oidc or private.
   */
  public function getType() {
    return $this->get('type');
  }

  /**
   * Get field name.
   *
   * @return string
   *   The field name.
   */
  public function getFieldName() {
    return $this->get('field_name');
  }

}
