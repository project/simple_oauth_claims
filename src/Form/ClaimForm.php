<?php

namespace Drupal\simple_oauth_claims\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_oauth_claims\Entity\Claim;

/**
 * Claim form.
 *
 * @property \Drupal\simple_oauth_claims\ClaimInterface $entity
 */
class ClaimForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the claim.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\simple_oauth_claims\Entity\Claim::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->entity->getType(),
      '#description' => $this->t('The claim type controls, where the claims are available, e.g. in the OpenID Connect response or the JWT payload.'),
      '#options' => [
        Claim::TYPE_UND => $this->t('Undetermined'),
        Claim::TYPE_OIDC => $this->t('OpenID Connect claim'),
        Claim::TYPE_PRIVATE => $this->t('Private claim'),
      ],
    ];

    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions('user', 'user');
    /** @var \Drupal\Core\Field\BaseFieldDefinition $field */
    foreach ($fields as $field) {
      $field_names[$field->getName()] = (string) $field->getLabel();
    }
    $form['field_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Field name'),
      '#options' => $field_names,
      '#default_value' => $this->entity->getFieldName(),
      '#description' => $this->t('The name of the field that is used to provide the data for the claim.'),
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new claim %label.', $message_args)
      : $this->t('Updated claim %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    // Trigger compiler run to take changes into effect.
    // @todo Anyone a better solution to rebuild service parameters?
    require DRUPAL_ROOT . '/core/includes/utility.inc';
    $class_loader = require DRUPAL_ROOT . '/autoload.php';
    \drupal_rebuild($class_loader, \Drupal::requestStack()->getCurrentRequest());

    return $result;
  }

}
