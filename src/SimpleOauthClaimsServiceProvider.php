<?php

namespace Drupal\simple_oauth_claims;

use Drupal\Core\Database\Database;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\simple_oauth_claims\Entity\Claim;

/**
 * Defines a service provider for the simple_oauth_claims module.
 */
class SimpleOauthClaimsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Workaround to access the database before containers have been built.
    // Hopefully the 'cache_config' table is there and not e.g.
    // relocated to some key-value store.
    $db = Database::getConnection('default');
    try {
      $claims = $db
        ->query("select cid, data from {cache_config} where cid LIKE 'simple_oauth_claims.claim.%'")
        ->fetchAll();
    }
    catch (\Exception $e) {
      // Table probably does not exist.
      return;
    }

    $claim_names = $container->getParameter('simple_oauth.openid.claims');

    // Add configured claim names.
    foreach ($claims as $claim) {
      $data = unserialize($claim->data);
      // Only consider OIDC claims here.
      if ($data['type'] == Claim::TYPE_OIDC
        || $data['type'] == Claim::TYPE_UND) {

        $claim_names[] = $data['id'];
        $container->setParameter('simple_oauth.openid.claims', $claim_names);
      }
    }
  }

}
