<?php

namespace Drupal\simple_oauth_claims;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a claim entity type.
 */
interface ClaimInterface extends ConfigEntityInterface {

}
