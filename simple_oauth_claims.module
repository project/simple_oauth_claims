<?php

/**
 * @file
 * Primary module hooks for Simple OAuth Claims module.
 */

use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\simple_oauth\Entities\AccessTokenEntity;
use Drupal\simple_oauth_claims\Entity\Claim;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Implements hook_simple_oauth_private_claims_alter().
 */
function simple_oauth_claims_simple_oauth_private_claims_alter(&$private_claims, AccessTokenEntity $access_token_entity) {
  $user_id = $access_token_entity->getUserIdentifier();
  $account = User::load($user_id);

  $result = \Drupal::entityQuery('claim')
    ->condition('status', TRUE)
    ->condition('type', ['private', 'undetermined'], 'IN')
    ->execute();
  $claims = Claim::loadMultiple($result);

  foreach ($claims as $claim) {
    $field_type = $account->get($claim->getFieldName())->getFieldDefinition()
      ->getType();
    $values = $account->get($claim->getFieldName())->getValue();

    $value = _simple_oauth_claims_prepare($field_type, $values);
    $private_claims[$claim->id()] = $value;
  }

  // Pre-populate iss claim if requested.
  if (isset($private_claims['iss'])) {
    $private_claims['iss'] = Url::fromUri('internal:/')->setAbsolute()->toString();
  }
}

/**
 * Implements hook_simple_oauth_oidc_claims_alter().
 */
function simple_oauth_claims_simple_oauth_oidc_claims_alter(array &$claim_values, array &$context) {
  $account = $context['account'];
  assert($account instanceof UserInterface);

  $result = \Drupal::entityQuery('claim')
    ->condition('status', TRUE)
    ->condition('type', ['oidc', 'undetermined'], 'IN')
    ->execute();
  $claims = Claim::loadMultiple($result);

  foreach ($claims as $claim) {
    $field_type = $account
      ->get($claim->getFieldName())
      ->getFieldDefinition()
      ->getType();
    $values = $account
      ->get($claim->getFieldName())
      ->getValue();

    $value = _simple_oauth_claims_prepare($field_type, $values);
    $claim_values[$claim->id()] = $value;
  }

  // Pre-populate iss claim if requested.
  if (isset($claim_values['iss'])) {
    $claim_values['iss'] = Url::fromUri('internal:/')->setAbsolute()->toString();
  }
}

/**
 * Prepare field values as claim values.
 *
 * @param string $field_type
 *   A string defining the field type.
 * @param array $values
 *   An array of field values.
 *
 * @return bool|int|string|null
 */
function _simple_oauth_claims_prepare($field_type, $values) {
  switch ($field_type) {
    case 'boolean':
      $item = reset($values);
      $value = (bool) ($item['value'] ?? FALSE);
      break;
    case 'integer':
    case 'created':
    case 'changed':
      $item = reset($values);
      $value = (int) ($item['value'] ?? 0);
      break;
    case 'entity_reference':
      $value = implode(',', array_values(reset($values)));
      break;
    case 'string':
    case 'email':
    case 'language':
    case 'password':
    case 'uuid':
      $item = reset($values);
      $value = (string) ($item['value'] ?? NULL);
      break;
    case 'path':
      $item = reset($values);
      $value = (string) ($item['alias'] ?? '');
      break;
    case 'image':
      $file = File::load(reset($values)['target_id']);
      $value = \Drupal::service('file_url_generator')
        ->generateAbsoluteString($file->getFileUri());
      break;
    case 'datetime':
    default:
      $item = reset($values);
      $value = (string) ($item['value'] ?? '');
      \Drupal::logger('simple_oauth_claims')
        ->warning('Field type <em>@field_type<em> could not be prepared.', [
          '@field_type' => $field_type,
        ]);
      break;
  }
  return $value;
}
